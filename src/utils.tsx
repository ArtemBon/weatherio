function getCurrentDate(timestampInSeconds: number, timezoneShiftInSeconds: number) {
  const weekDayNames = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
  const monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

  const currentDate = new Date((timestampInSeconds + timezoneShiftInSeconds) * 1000);
  const currentWeekDayName = weekDayNames[currentDate.getUTCDay()];
  const currentMonthName = monthNames[currentDate.getUTCMonth()];

  return `${currentWeekDayName} ${currentDate.getUTCDate()}, ${currentMonthName}`;
}

function getCurrentTime(timestampInSeconds: number, timezoneShiftInSeconds: number) {
  const currentDate = new Date((timestampInSeconds + timezoneShiftInSeconds) * 1000);

  return `${currentDate.getUTCHours()}:${currentDate.getUTCMinutes()}`;
}

function kelvinToCelsius(kelvin: number) {
  return Math.round(kelvin - 273.15);
}

export { getCurrentDate, getCurrentTime, kelvinToCelsius };
