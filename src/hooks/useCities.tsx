import { FormEvent, useState } from 'react';
import apiClient, { CanceledError } from '../services/api-client';

export interface City {
  name: string;
  state: string;
  country: string;
  lat: number;
  lon: number;
}

function useCities() {
  const [searchText, setSearchText] = useState('');
  const [cities, setCities] = useState<City[]>([]);

  function onInput(event: FormEvent<HTMLInputElement>) {
    const userInput = event.currentTarget.value;
    setSearchText(userInput);

    if (userInput) {
      apiClient.get<City[]>('/geo/1.0/direct', { params: { limit: 5, q: userInput } })
        .then(res => {
          setCities(res.data);
        })
        .catch(err => {
          if (err instanceof CanceledError) return;
          setCities([]);
        });
    } else {
      setCities([]);
    }
  }

  return { searchText, cities, setCities, onInput };
}

export default useCities;
