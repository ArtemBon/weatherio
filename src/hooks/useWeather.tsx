import { useEffect, useState } from "react";
import apiClient, { CanceledError } from "../services/api-client";

interface MainWeather {
  temp: number;
}

interface Weather {
  main: string;
  icon: string;
}

interface SystemData {
  country: string;
}

export interface WeatherFetchData {
  main: MainWeather;
  weather: Weather[];
  sys: SystemData;
  name: string;
  dt: number;
  timezone: number;
}

export interface Coordinates {
  lat: number;
  lon: number;
}

function useWeather(coordinates: Coordinates) {
  const [weather, setWeather] = useState<WeatherFetchData | null>(null);
  
  useEffect(() => {
    const controller = new AbortController();

    apiClient.get<WeatherFetchData>('/data/2.5/weather', { signal: controller.signal, params: { ...coordinates } })
      .then(res => {
        console.log(res.data)
        setWeather(res.data)
      })
      .catch(err => {
        if (err instanceof CanceledError) return;
      });

    return () => controller.abort();
  }, [coordinates]);

  return weather;
}

export default useWeather;
