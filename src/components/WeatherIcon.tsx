import { Image } from '@chakra-ui/react';
import icon01d from '../assets/weather-icons/01d.png';
import icon01n from '../assets/weather-icons/01n.png';
import icon02d from '../assets/weather-icons/02d.png';
import icon02n from '../assets/weather-icons/02n.png';
import icon03d from '../assets/weather-icons/03d.png';
import icon03n from '../assets/weather-icons/03n.png';
import icon04d from '../assets/weather-icons/04d.png';
import icon04n from '../assets/weather-icons/04n.png';
import icon09d from '../assets/weather-icons/09d.png';
import icon09n from '../assets/weather-icons/09n.png';
import icon10d from '../assets/weather-icons/10d.png';
import icon10n from '../assets/weather-icons/10n.png';
import icon11d from '../assets/weather-icons/11d.png';
import icon11n from '../assets/weather-icons/11n.png';
import icon13d from '../assets/weather-icons/13d.png';
import icon13n from '../assets/weather-icons/13n.png';
import icon50d from '../assets/weather-icons/50d.png';
import icon50n from '../assets/weather-icons/50n.png';

interface Props {
  icon: string;
}

function WeatherIcon({ icon }: Props) {
  const iconMap: { [key: string]: string } = {
    '01d': icon01d, '01n': icon01n, '02d': icon02d, '02n': icon02n, '03d': icon03d, '03n': icon03n,
    '04d': icon04d, '04n': icon04n, '09d': icon09d, '09n': icon09n, '10d': icon10d, '10n': icon10n,
    '11d': icon11d, '11n': icon11n, '13d': icon13d, '13n': icon13n, '50d': icon50d, '50n': icon50n
  };
  
  return (
    <Image src={iconMap[icon]} boxSize={16}/>
  );
}

export default WeatherIcon;
