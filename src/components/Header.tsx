import { Box, Button, Heading, HStack, Image, Show } from '@chakra-ui/react';
import { TbCurrentLocation } from 'react-icons/tb';
import Search from './search/Search';
import logo from '../assets/logo.png';
import { Coordinates } from '../hooks/useWeather';

interface Props {
  setCoordinates: (coordinates: Coordinates) => void;
}

function Header({ setCoordinates }: Props) {
  function handleCurrentLocation() {
    navigator.geolocation.getCurrentPosition((position) => {
      setCoordinates({ lat: position.coords.latitude, lon: position.coords.longitude });
    });
  }

  return (
    <HStack height={{ base: 20, md: 24 }} justifyContent='space-between'>
      <HStack>
        <Image src={logo} boxSize={{ base: 10, xl: 16 }} />
        <Heading fontSize={{ base: 20, xl: 32 }}>weatherio</Heading>
      </HStack>
      <Search onCityClick={setCoordinates}/>
      <Button
        onClick={handleCurrentLocation}
        padding={3}
        borderRadius={30}
        colorScheme='purple'
      >
        <Box marginRight={{ md: 2 }}>
          <TbCurrentLocation />
        </Box>
        <Show above='md'>Current location</Show>
      </Button>
    </HStack>
  );
}

export default Header;
