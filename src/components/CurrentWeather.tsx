import { Card, CardBody, Divider, HStack, Heading, Text } from '@chakra-ui/react';
import { AiOutlineCalendar } from 'react-icons/ai';
import { GoLocation } from 'react-icons/go';
import { getCurrentDate, getCurrentTime, kelvinToCelsius } from '../utils';
import { WeatherFetchData } from '../hooks/useWeather';
import WeatherIcon from './WeatherIcon';

interface Props {
  weather: WeatherFetchData | null;
}

function CurrentWeather({ weather }: Props) {
  if (!weather) return null;

  return (
    <Card borderRadius={16}>
      <CardBody>
        <Heading as='h3' fontSize={20} marginBottom={3}>
          Now {getCurrentTime(weather.dt, weather.timezone)}
        </Heading>
        <HStack justifyContent='space-between'>
          <Text fontSize={56}>{kelvinToCelsius(weather.main.temp)}°C</Text>
          <WeatherIcon icon={weather.weather[0].icon} />
        </HStack>
        <Text marginBottom={3}>{weather.weather[0].main}</Text>
        <Divider orientation='horizontal' marginBottom={3} />
        <HStack>
          <AiOutlineCalendar />
          <Text color='gray.400'>{getCurrentDate(weather.dt, weather.timezone)}</Text>
        </HStack>
        <HStack>
          <GoLocation />
          <Text color='gray.400'>{`${weather.name}, ${weather.sys.country}`}</Text>
        </HStack>
      </CardBody>
    </Card>
  );
}

export default CurrentWeather;
