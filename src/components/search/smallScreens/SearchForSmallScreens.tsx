import { Box, Button, Divider, HStack, useDisclosure } from '@chakra-ui/react';
import { FormEvent } from 'react';
import { AiOutlineSearch } from 'react-icons/ai';
import { IoMdArrowBack } from 'react-icons/io';
import { City } from '../../../hooks/useCities';
import InputField from './InputField';
import SuggestionsList from '../SuggestionsList';

interface Props {
  cities: City[];
  onCityClick: (city: City) => void;
  onInput: (event: FormEvent<HTMLInputElement>) => void;
  searchText: string;
}

function SearchForSmallScreens({ cities, onCityClick, onInput, searchText }: Props) {
  const { isOpen, onOpen, onClose } = useDisclosure();

  function handleCityClick(city: City) {
    onClose();
    onCityClick(city);
  }

  return (
    <Box width={{ base: '100%', md: '60%', lg: '65%' }}>
      <Button onClick={onOpen} padding={3} borderRadius={30} backgroundColor='gray.700' float='right'>
        <AiOutlineSearch />
      </Button>
      <Box
        position="fixed"
        top={0}
        left="-0.5rem"
        right={0}
        bottom={0}
        backgroundColor="gray.700"
        zIndex='overlay'
        display={isOpen ? 'block' : 'none'}
      >
        <HStack paddingY={3}>
          <Button
            onClick={onClose}
            borderRadius={0}
            backgroundColor='transparent'
            _hover={{ backgroundColor: 'none' }}
            _active={{ backgroundColor: 'none'}}
          >
            <IoMdArrowBack />
          </Button>
          <InputField onInput={onInput} searchText={searchText} />
        </HStack>
        <Divider />
        {cities.length !== 0 && (
          <SuggestionsList cities={cities} onCityClick={handleCityClick} />
        )}
      </Box>
    </Box>
  );
}

export default SearchForSmallScreens;
