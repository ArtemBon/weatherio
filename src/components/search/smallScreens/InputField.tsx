import { Input } from '@chakra-ui/react';
import { FormEvent } from 'react';

interface Props {
  onInput: (event: FormEvent<HTMLInputElement>) => void;
  searchText: string;
}

function InputField({ onInput, searchText }: Props) {
  return (
    <Input
      onInput={onInput}
      value={searchText}
      backgroundColor='gray.700'
      placeholder='Search city...'
      _placeholder={{ color: 'gray.300' }}
      variant='filled'
      border='none'
      _hover={{ backgroundColor: 'transparent' }}
      padding={0}
    />
  );
}

export default InputField;
