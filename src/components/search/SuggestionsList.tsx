import { Box, HStack, Heading, Icon, List, ListItem, ListProps, Text } from '@chakra-ui/react';
import { GoLocation } from 'react-icons/go';
import { City } from '../../hooks/useCities';

interface Props {
  cities: City[];
  listProperties?: ListProps; 
  onCityClick: (city: City) => void;
}

function SuggestionsList({ cities, listProperties = {}, onCityClick }: Props) {
  return (
    <List {...listProperties}>
      {cities.map(city => (
        <ListItem
          key={`${city.lat} ${city.lon}`}
          padding={2}
          _hover={{ backgroundColor: 'gray.600' }}
          cursor='pointer'
          onClick={() => onCityClick(city)}
        >
          <HStack>
            <Icon color='gray.400' boxSize={5} as={GoLocation} margin={2} />
            <Box>
              <Heading as='h6' fontSize={16}>{city.name}</Heading>
              <Text color='gray.400' fontSize={12}>{city.state} {city.country}</Text>
            </Box>
          </HStack>
        </ListItem>
      ))}
    </List>
  );
}

export default SuggestionsList;
