import { Hide, Show } from '@chakra-ui/react';
import useCities, { City } from '../../hooks/useCities';
import { Coordinates } from '../../hooks/useWeather';
import SearchForLargeScreens from './largeScreens/SearchForLargeScreens';
import SearchForSmallScreens from './smallScreens/SearchForSmallScreens';

interface Props {
  onCityClick: (coordinates: Coordinates) => void;
}

function Search({ onCityClick }: Props) {
  const { searchText, cities, setCities, onInput } = useCities();

  function handleCityClick(city: City) {
    setCities([]);
    onCityClick({ lat: city.lat, lon: city.lon });
  }

  return (
    <>
      <Show above='xl'>
        <SearchForLargeScreens cities={cities} onInput={onInput} searchText={searchText} onCityClick={handleCityClick} />
      </Show>
      <Hide above='xl'>
        <SearchForSmallScreens cities={cities} onInput={onInput} searchText={searchText} onCityClick={handleCityClick} />
      </Hide>
    </>
  );
}

export default Search;
