import { Input, InputGroup, InputLeftElement } from '@chakra-ui/react';
import { FormEvent } from 'react';
import { AiOutlineSearch } from 'react-icons/ai';
import { City } from '../../../hooks/useCities';

interface Props {
  cities: City[];
  onInput: (event: FormEvent<HTMLInputElement>) => void;
  searchText: string;
}

function InputField({ cities, onInput, searchText }: Props) {
  return (
    <InputGroup>
      <InputLeftElement pointerEvents='none' children={<AiOutlineSearch />} />
      <Input
        onInput={onInput}
        value={searchText}
        variant='filled'
        backgroundColor='gray.700'
        placeholder='Search city...'
        _placeholder={{ color: 'gray.300' }}
        _hover={{ backgroundColor: 'gray.700' }}
        _focus={{ backgroundColor: 'gray.700' }}
        border='none'
        borderTopRadius='18px'
        borderBottomRadius={cities.length ? 'none' : '18px'}
      />
    </InputGroup>
  );
}

export default InputField;
