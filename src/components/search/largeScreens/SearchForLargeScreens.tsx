import { ListProps, Stack } from '@chakra-ui/react';
import { City } from '../../../hooks/useCities';
import { FormEvent } from 'react';
import InputField from './InputField';
import SuggestionsList from '../SuggestionsList';

interface Props {
  cities: City[];
  onCityClick: (city: City) => void;
  onInput: (event: FormEvent<HTMLInputElement>) => void;
  searchText: string;
}

const listProperties = {
  backgroundColor: 'gray.700',
  position: 'fixed',
  top: '59px',
  width: '600px',
  paddingY: 3,
  borderTop: '1px',
  borderColor: 'gray.400',
  borderBottomRadius: '18px',
  zIndex: 'dropdown',
  boxShadow: 'dark-lg',
} as ListProps

function SearchForLargeScreens({ cities, onCityClick, onInput, searchText }: Props) {

  return (
    <Stack width='600px'>
      <InputField cities={cities} onInput={onInput} searchText={searchText} />
      {cities.length && (
        <SuggestionsList listProperties={listProperties} cities={cities} onCityClick={onCityClick} />
      )}
    </Stack>
  );
}

export default SearchForLargeScreens;
