import { Stack } from '@chakra-ui/react';
import CurrentWeather from './CurrentWeather';
import { WeatherFetchData } from '../hooks/useWeather';

interface Props {
  weather: WeatherFetchData | null;
}

function Aside({ weather }: Props) {
  return (
    <Stack>
      <CurrentWeather weather={weather} />
    </Stack>
  );
}

export default Aside;
