import axios, { CanceledError } from "axios";

export { CanceledError };

const instance =  axios.create({
  baseURL: 'https://api.openweathermap.org'
});

instance.interceptors.request.use(config => {
  config.params = {
    appid: 'fc95c9d6d60cb545508472872f2fb2d1',
    ...config.params
  };

  return config;
});

export default instance;
