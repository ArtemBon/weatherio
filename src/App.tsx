import { Grid, GridItem } from '@chakra-ui/react';
import Header from './components/Header';
import Aside from './components/Aside';
import useWeather, { Coordinates } from './hooks/useWeather';
import { useState } from 'react';

function App() {
  const [coordinates, setCoordinates] = useState<Coordinates>({ lat: 51.5085, lon: -0.1257 })
  const weather = useWeather(coordinates);

  return (
    <Grid
      templateAreas={{
        base: `"header" "aside" "main" "footer"`,
        md: `"header header" "aside  main" "aside  footer"`
      }}
      gridTemplateColumns={{ base: '1fr', md: '250px 1fr', lg: '350px 1fr', xl: '430px 1fr' }}
      gap={{ base: 6, lg: 8, xl: 10 }}
      paddingX={{ base: 6, lg: 8, xl: 10 }}
    >
      <GridItem area={'header'}>
        <Header setCoordinates={setCoordinates} />
      </GridItem>
      <GridItem area={'aside'}>
        <Aside weather={weather} />
      </GridItem>
      <GridItem pl='2' bg='green.300' area={'main'}>
        Main
      </GridItem>
      <GridItem pl='2' bg='blue.300' area={'footer'}>
        Footer
      </GridItem>
    </Grid>
  );
}

export default App;
