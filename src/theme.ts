import { extendTheme, type ThemeConfig } from '@chakra-ui/react';

const config: ThemeConfig = {
  initialColorMode: 'dark',
  useSystemColorMode: false,
}

const theme = extendTheme({
  config,
  colors: {
    gray: {
      50: '#d7d7d7',
      100: '#bcbbbe',
      200: '#a2a0a5',
      300: '#87848d',
      400: '#6e6b74',
      500: '#555359',
      600: '#3d3c3f',
      700: '#242426',
      800: '#0c0c0e',
      900: '#131215'
    }
  },
  fonts: {
    heading: `'Nunito Sans', sans-serif`,
    body: `'Nunito Sans', sans-serif`
  }
});

export default theme;
